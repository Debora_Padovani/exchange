import React, { useState } from 'react'
import styled from 'styled-components'
import { Svg } from '@pancakeswap-libs/uikit'
import { useHistory } from 'react-router-dom';


import links from './config'

const MenuDiv = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  @media screen and (max-width: 767px){
    order: -1;
  }
`;

const MenuButton = styled.button`
  display: none;
  background: none;
  border: none;
  font-size: 35px;
  svg{
    fill: #000;
    font-size: 20px;
    width: 2rem;
    height: 2rem;
  }
  @media screen and (max-width: 767px){
    display: flex;
    align-items: center;
  }

`;

const MenuUl = styled.ul`
  display: inline-flex;
  justify-content: flex-start;
  align-items: center;
  @media screen and (max-width: 767px){
    background: #fefbf1;
    position: absolute;
    left: 0;
    margin-left: 0;
    transform: translateX(-100%);
    width: 100vw;
    height: 100vh;
    display: block;
    top: 0;
    transition: 1500ms;
    &.opened{
      transform: translateX(0);
    }
  }
`;

const MenuLi = styled.li`
  list-style: none;
  margin: 1rem;
  &::before{
    content: none;
  }
  &.closeButton{
    display: none;
    font-size: 21px;
    font-weight: 600;
    text-align: right;
    padding-right: 2rem;
    color: #f0ba02;
  }
  @media screen and (max-width: 767px){
    padding: 1.2rem;
    border-bottom: 1px solid #eceae0;
    margin: 0;
    font-size: 18px;
    &.closeButton{
      display: block;
    }
  }
`;

const MenuA = styled.a`
  color: #000;
  font-weight: 600;
  text-transform: uppercase;
  &:hover{
    color: #666;
  }
  @media screen and (max-width: 767px){
    letter-spacing: 1px;
  }
`;


const Menu: React.FC = (props) => {

  const [menuIsOpened, setMenuIsOpened] = useState(false);
  const history = useHistory();

  return (
    <MenuDiv>
      <MenuButton 
          onClick={() => setMenuIsOpened(true)}
      >
        <Svg viewBox="0 0 24 24" 
          fill="none" 
          stroke="currentColor" 
          stroke-width="2" 
          stroke-linecap="round" 
          stroke-linejoin="round" 
        >
          <line x1="3" y1="12" x2="21" y2="12"/>
          <line x1="3" y1="6" x2="21" y2="6"/>
          <line x1="3" y1="18" x2="21" y2="18"/>
        </Svg>

      </MenuButton>
      <MenuUl className={`${menuIsOpened ? "opened" : ""}`}>
      <MenuLi 
        className="closeButton"
        onClick={() => setMenuIsOpened(false)}
      > 
          X
      </MenuLi>
      {links.map((link) =>{
        return (
          <MenuLi>
            <MenuA 
              onClick={() => {
                history.push(`${link.href}`);
                setMenuIsOpened(false);
              }}
              key={link.id}>
            {link.label}
            </MenuA>
          </MenuLi>
        );
      })}  
      </MenuUl>
    </MenuDiv>
  )
}

export default Menu
