const config = [
  {
    label: 'Dashboard',
    href: '/dashboard',
    id: 1,
  },
  {
    label: 'Swap',
    href: '/swap',
    id: 2,
  },
  {
    label: 'Liquidity',
    href: '/pool',
    id: 3,
  },
  {
    label: 'Farm',
    href: '/farm',
    id: 4,
  },
]

export default config
