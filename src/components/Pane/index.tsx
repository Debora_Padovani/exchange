import styled from 'styled-components'

const Pane = styled.div`
  border: 1px solid #F0BA03;
  border-radius: 5px;
  padding: 20px;
  background-color: #F0BA0310;
  color: #F0BA03 !important;
  font-size: 14px !important;
`

export default Pane
