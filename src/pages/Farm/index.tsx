import React from 'react'
import styled from 'styled-components'

const StyledImg = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: calc(100vh - 136px);
    top: 50px;
    img{
        max-width: 100%;
        max-height: 100%;
        object-fit: cover;
    }
`

export default function Farm() {

  return (

    <StyledImg >
        <img src="/images/coming-soon.jpg" alt="coming soon"/>
    </StyledImg>
  )
}
