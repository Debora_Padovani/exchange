# Vitalik Nakamoto Coin

This repo is responsible for the **exchange** interface of the AMM: [exchange.vitalinknakamoto.io](https://exchange.vitaliknakamoto.io/)

If you want to contribute, please refer to the [contributing guidelines](./CONTRIBUTING.md) of this project.
